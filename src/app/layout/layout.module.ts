import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PlayersComponent } from './players/players.component';
import { TeamsComponent } from './teams/teams.component';
import { OwnersComponent } from './owners/owners.component';
import { AuctionComponent } from './auction/auction.component';
import { EventsComponent } from './events/events.component';
import { MaterialDesignModule } from '../material-design/material-design.module';
import { RouterModule } from '@angular/router';
import { LayoutRoutes } from './layout-routing.module';
import { FlexLayoutModule } from "@angular/flex-layout";
import { PlayerComponent } from './players/player/player.component';
import { PlayerService } from '../shared/player.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';





@NgModule({
  declarations: [LayoutComponent, SidebarComponent, HeaderComponent, FooterComponent, DashboardComponent, PlayersComponent, TeamsComponent, OwnersComponent, AuctionComponent, EventsComponent, PlayerComponent],
  imports: [
    CommonModule,
    MaterialDesignModule,
    RouterModule.forChild(LayoutRoutes),
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    
  ],
  entryComponents:[
    PlayerComponent,
  ],
  providers:[
    PlayerService
  ]
})
export class LayoutModule { }
