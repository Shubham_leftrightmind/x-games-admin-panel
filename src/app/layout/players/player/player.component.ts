import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { PlayerService } from 'src/app/shared/player.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  constructor(
    private playerService: PlayerService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }
  onClear(){
    this.playerService.form.reset();
  }
  onClose(){
    this.playerService.form.reset();

  }
}
