import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { PlayerComponent } from './player/player.component';
import { Players } from 'src/app/models/players.model';
import { PlayerService } from 'src/app/shared/player.service';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';


@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {
                                            
 player:Players [] = [];

 players1:any=[]

  displayedColumns: string[] = ['id', 'name', 'actions'];
  dataSource: MatTableDataSource<Players>;

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  
  //@ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  public config: PerfectScrollbarConfigInterface = {};

  constructor(
    public dialog: MatDialog,
    public playerService: PlayerService
  ) {
    this.dataSource = new MatTableDataSource(this.players1);
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
   // this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.playerService.getPlayers().subscribe((snap:any[])=>{
     this.players1 = snap
     this.dataSource= this.players1;
     snap.forEach(element => {
      this.player= element;
      });
    })  
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

    //Add Player
    onCreate(){
      const dialogConfig = new MatDialogConfig();
           // dialogConfig.disableClose = true;
            dialogConfig.autoFocus = true;
            dialogConfig.width = "60%";
            this.dialog.open(PlayerComponent,dialogConfig);

    }
}
