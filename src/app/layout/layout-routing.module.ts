import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EventsComponent } from './events/events.component';
import { AuctionComponent } from './auction/auction.component';
import { PlayersComponent } from './players/players.component';
import { TeamsComponent } from './teams/teams.component';
import { OwnersComponent } from './owners/owners.component';

export const LayoutRoutes: Routes = [
  {
    path:'',
    redirectTo: '/dashboard',
    pathMatch:'full'
  },
  { path: 'dashboard',      component: DashboardComponent },
  { path: 'events',      component: EventsComponent },
  { path: 'auction',      component: AuctionComponent },
  { path: 'players',      component: PlayersComponent },
  { path: 'teams',      component: TeamsComponent },
  { path: 'owners',      component: OwnersComponent },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(LayoutRoutes)
  ],
  exports:[
    RouterModule
  ]
})
export class LayoutRoutingModule { }
