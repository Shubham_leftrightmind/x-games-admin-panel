import { Input, Component, Output, EventEmitter,OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../shared/login.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar,   MatSnackBarConfig } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  hide = true;
  message:string='Welcome to X-Games';
  setAutoHide: boolean = true;
  autoHide: number = 2000;
  
  @Input() error: string | null;

  @Output() submitEM = new EventEmitter();


  constructor(
    private loginService: LoginService,
    private router: Router,
    private snackBar: MatSnackBar
    ) { }

  ngOnInit() {
    this.initForm();
  }

  
  initForm(){
    this.loginForm = new FormGroup({
      username: new FormControl('',[Validators.required]),
      password: new FormControl('',[Validators.required]),
    });
}


  OnSubmit(){
    if (this.loginForm.valid) {
          this.loginService.login(this.loginForm.value).subscribe(result=>{
            if(result.statusCode === 200){
              console.log("login success",result);
              let config = new MatSnackBarConfig();
              config.duration = this.setAutoHide ? this.autoHide : 0;
              this.snackBar.open(this.message,'',config);
              this.router.navigate(['/dashboard']);
            }
            else{
              alert("Username or password is incorrect");
              
            }
          })
        }
 }

}
