import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private Login_API: any = "http://10.10.10.112:8080/xgames/user/login/" ;
  constructor(private http: HttpClient) { }

  login(data):Observable <any>{
    const url = this.Login_API + data.username + '/' + data.password;
    return this.http.get(url);
  }

}
