import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private Player_API ="http://10.10.10.112:8080/xgames/player/all";
  constructor(private http: HttpClient) { }

  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    contact_no: new FormControl('', [Validators.required, Validators.minLength(8)]),
    dob: new FormControl(''),
    blood_group: new FormControl('', Validators.required),
    profile_pic: new FormControl(''),
    gender: new FormControl('1'),
    
  });

  //Error Handling

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getPlayers(){
    return this.http.get(this.Player_API).pipe(catchError(this.handleError));
  }
}
