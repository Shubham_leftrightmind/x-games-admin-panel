export class Players{
    $key: any;
    playerId:number;
    password: string;
    firstName:string;
    fullName:string;
    email:string;
    contact_no:number;
    dob:string;
    blood_group:string;
    gender:boolean;
}
