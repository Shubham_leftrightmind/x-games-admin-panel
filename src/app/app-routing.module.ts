import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './layout/dashboard/dashboard.component';


const routes: Routes = [
  {
    path:'',
    redirectTo: '/login',
    pathMatch:'full'
  },
  {
    path:'login', component:LoginComponent
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, 
  {
    path: '',
    component: LayoutComponent,
    children: [{
      path: '',
      loadChildren: './layout/layout.module#LayoutModule'
    }]
  }
];

@NgModule({
  imports: [
  //  RouterModule.forRoot(routes)
  RouterModule.forRoot(routes,{
    useHash: true
 })
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
